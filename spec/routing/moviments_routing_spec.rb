require "rails_helper"

RSpec.describe MovimentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/moviments").to route_to("moviments#index")
    end

    it "routes to #new" do
      expect(:get => "/moviments/new").to route_to("moviments#new")
    end

    it "routes to #show" do
      expect(:get => "/moviments/1").to route_to("moviments#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/moviments/1/edit").to route_to("moviments#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/moviments").to route_to("moviments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/moviments/1").to route_to("moviments#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/moviments/1").to route_to("moviments#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/moviments/1").to route_to("moviments#destroy", :id => "1")
    end
  end
end
