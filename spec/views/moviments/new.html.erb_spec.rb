require 'rails_helper'

RSpec.describe "moviments/new", type: :view do
  before(:each) do
    assign(:moviment, Moviment.new(
      :product_id => 1,
      :store_id => 1,
      :quantity => 1,
      :price_sell => "9.99",
      :price_cost => "9.99"
    ))
  end

  it "renders new moviment form" do
    render

    assert_select "form[action=?][method=?]", moviments_path, "post" do

      assert_select "input[name=?]", "moviment[product_id]"

      assert_select "input[name=?]", "moviment[store_id]"

      assert_select "input[name=?]", "moviment[quantity]"

      assert_select "input[name=?]", "moviment[price_sell]"

      assert_select "input[name=?]", "moviment[price_cost]"
    end
  end
end
