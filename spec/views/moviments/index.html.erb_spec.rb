require 'rails_helper'

RSpec.describe "moviments/index", type: :view do
  before(:each) do
    assign(:moviments, [
      Moviment.create!(
        :product_id => 2,
        :store_id => 3,
        :quantity => 4,
        :price_sell => "9.99",
        :price_cost => "9.99"
      ),
      Moviment.create!(
        :product_id => 2,
        :store_id => 3,
        :quantity => 4,
        :price_sell => "9.99",
        :price_cost => "9.99"
      )
    ])
  end

  it "renders a list of moviments" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
