require 'rails_helper'

RSpec.describe "moviments/show", type: :view do
  before(:each) do
    @moviment = assign(:moviment, Moviment.create!(
      :product_id => 2,
      :store_id => 3,
      :quantity => 4,
      :price_sell => "9.99",
      :price_cost => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
  end
end
