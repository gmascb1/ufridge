# spec/integration/mailer_spec.rb
# rake rswag:specs:swaggerize
require 'swagger_helper'

describe 'API' do

  path '/api/mailer/send_mail' do

    post 'Send Mail' do
      tags 'Mailer'
      consumes 'application/json'

      parameter name: 'Authorization', :in => :header, :type => :string

      parameter name: :request, in: :body, schema: {
          type: :object,
          properties: {
              email: {type: :string},
              moviment_ids: {
                  type: :array,
                  items: {type: :integer}
              }
          }
      }


      response '200', 'Mail sended' do
        run_test!
      end

      response '401', 'Unauthorize' do
        run_test!
      end
    end
  end

end