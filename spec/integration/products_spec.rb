# spec/integration/products_spec.rb
# rake rswag:specs:swaggerize
require 'swagger_helper'

describe 'API' do

  path '/api/products' do

    get 'Get Products' do
      tags 'Products'
      consumes 'application/json', 'application/xml'

      parameter name: 'Authorization', :in => :header, :type => :string
      parameter name: 'category', :in => :query, :type => :string

      response '200', 'products found' do

        user = User.find_or_initialize_by(email: "email@email.com")
        user.save!(validate: false)
        
        let(:product) { Product.create(name: 'foo', description: 'bar', price_sell: 2, price_cost: 1, unity: 1) }
        let(:category) { "Bebida" }
        let(:Authorization) { "Token token=#{User.last.api_key}" }

        run_test!
      end

      response '401', 'Unauthorize' do
        let(:category) { "Bebida" }
        let(:Authorization) { "Token token=" }
        run_test!
      end
    end
  end

  path '/api/products/{id}' do

    get 'Retrieves a product' do
      tags 'Products'
      produces 'application/json', 'application/xml'
      parameter name: :id, :in => :path, :type => :string
      parameter name: 'Authorization', :in => :header, :type => :string

      response '200', 'Product found' do
        schema type: :object,
               properties: {
                   id: {type: :integer},
                   name: {type: :string},
                   description: {type: :string},
                   price_sell: {type: :string},
                   unity: {type: :string},
                   avatar: {type: :string},
               },
               required: ['id', 'name']

        user = User.find_or_initialize_by(email: "email@email.com")
        user.save!(validate: false)
        let(:Authorization) { "Token token=#{User.last.api_key}" }

        let(:id) { Product.create!(name: 'foo', description: 'bar', category: "Bebida", price_sell: 2, price_cost: 1, unity: 1).id }
        run_test!
      end

      response '404', 'product not found' do
        
        schema type: :object,
               properties: {
                   status: {type: :number},
                   messages: {type: :string}
               }
        
        user = User.find_or_initialize_by(email: "email@email.com")
        user.save!(validate: false)
        let(:Authorization) { "Token token=#{User.last.api_key}" }
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end

  path '/api/products/buy' do
    post 'Buy products' do
      tags 'Products'
      consumes 'application/json'
      parameter name: 'Authorization', :in => :header, :type => :string
      parameter name: :params, :in => :body, schema: {
        properties: {
          buy: {
            type: :array,
            items: {
              properties: {
                id: {
                    type: :string,
                    description: "product_id",
                    example: "1",

                },
                quantity: {
                    type: :string,
                    description: "quantity",
                    example: "1"
                },
                store_id: {
                    type: :string,
                    description: "store_id",
                    example: "1"
                }
              }
            }
          }
        }
      }

      response '200', 'OK' do
     
        schema type: :array,
               items: {
                 type: :integer
               }

        user = User.find_or_initialize_by(email: "email@email.com")
        user.save!(validate: false)

        product = Product.find_or_create_by!(name: "Produto A", category: "A", price_sell: 3, price_cost: 2)
        store = Store.find_or_create_by!(name: "Loja A")

        let(:Authorization) { "Token token=#{User.last.api_key}" }
        let(:params) {
          {
            "buy": [
              {
                "id": "#{product.id}",
                "quantity": "1",
                "store_id": "#{store.id}"
              }
            ]
          }
        }


        run_test!
      end

    end
  end

end