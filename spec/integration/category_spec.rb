# spec/integration/categories_spec.rb
# rake rswag:specs:swaggerize
require 'swagger_helper'

describe 'API' do

  path '/api/categories' do

    get 'Get Categories' do
      tags 'Categories'
      consumes 'application/json', 'application/xml'

      parameter name: 'Authorization', :in => :header, :type => :string

      response '200', 'categories found' do

        schema type: :array,
               items: {
                   type: :object,
                   properties: {
                       id: {
                         type: :integer,
                         example: "1",
                         description: "Identificador interno"
                        },
                       name: {
                         type: :string,
                         example: "Biscoito",
                         description: "Nome da categoria"
                      },
                   }
               }

        let(:Authorization) { "Token token=#{User.first.api_key}" }

        run_test!
      end

      response '401', 'Unauthorize' do
        let(:Authorization) { "" }
        run_test!
      end
    end
  end

end