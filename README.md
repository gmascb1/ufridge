# README

### Ruby version
2.5.7p206

### System dependencies
* brew install nodejs
* brew install yarn
* rails webpacker:install

### Configuration

### Database creation
* https://medium.com/@Umesh_Kafle/postgresql-and-postgis-installation-in-mac-os-87fa98a6814d
* brew install postgres
* brew install postgis

* start pg
  * pg_ctl -D /usr/local/var/postgres start
  * brew services start postgresql

* install dbeaver: https://dbeaver.io/download/


#### Passo a passo para permissoes da API
* Documentacao para auth
  * https://sourcey.com/articles/building-the-perfect-rails-api

### Gerar o swagger
* rake rswag:specs:swaggerize

### assets precompile
* rake assets:precompile RAILS_ENV=development