class CreateMoviments < ActiveRecord::Migration[6.0]
  def change
    create_table :moviments do |t|
      t.integer :product_id
      t.integer :store_id
      t.integer :quantity
      t.decimal :price_sell
      t.decimal :price_cost

      t.timestamps
    end
  end
end
