class CategorySerializer < ActiveModel::Serializer
  attributes :id
  attribute :name
end