class ProductSerializer < ActiveModel::Serializer
    attribute :id
    attribute :name
    attribute :description
    attribute :price_sell
    attribute :unity
    attribute :avatar

    def avatar
        object.try(:avatar).try(:path).nil? && result.try(:avatar).try(:file).nil? ? "" : Base64.encode64(File.read(object.avatar.path))
    rescue
        ""
    end

  end