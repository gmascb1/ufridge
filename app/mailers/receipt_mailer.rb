class ReceiptMailer < ApplicationMailer

  def receipt_email(email, ids)
    @ids = ids
    @email = email
    mail(to: email, subject: 'Comprovante de Pagamento')
  end

end
