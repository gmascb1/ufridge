class HomeController < ApplicationController
  before_action :set_filter

  def index

     data_quantity = ActiveRecord::Base.connection.execute("select concat(concat(s.name, ' - '), m.created_at::date) loja_data, sum(m.quantity) quantity
                                                from moviments m
                                                join stores s on (m.store_id = s.id)
                                                join products p on (m.product_id = p.id)
                                                where ((now()::date - m.created_at::date) <= 30)
                                                and m.origin = 'api'
                                                #{@filter}
                                                group by concat(concat(s.name, ' - '), m.created_at::date), m.created_at
                                                order by m.created_at")

    @chart_qtd_loja_dia = Dashboard.cubo_uma_serie(data_quantity,"Quantidade de vendas por dia", "loja_data", "Quantidade de itens", "Quantidade", "quantity", "#006e03")

    data_price_sell= ActiveRecord::Base.connection.execute("select concat(concat(s.name, ' - '), m.created_at::date) loja_data, sum(m.price_sell * m.quantity) price_sell
                                                from moviments m
                                                join stores s on (m.store_id = s.id)
                                                join products p on (m.product_id = p.id)
                                                where ((now()::date - m.created_at::date) <= 30)
                                                and m.origin = 'api'
                                                #{@filter}
                                                group by concat(concat(s.name, ' - '), m.created_at::date), m.created_at
                                                order by m.created_at")

    @chart_valor_venda_loja_dia = Dashboard.cubo_uma_serie(data_price_sell,"Valores das vendas por dia", "loja_data", "Valor da venda", "R$", "price_sell", "#941212")

   entrada_saida_produto = ActiveRecord::Base.connection
                               .execute("select concat(concat(s.name, ' - '), p.name) produto_loja, sum(m_entrada.quantity) - sum(m_saida.quantity) quantidade_em_loja
                                        -- movimentacoes de entrada
                                        from (select m.store_id, m.product_id, sum(m.quantity) quantity
                                            from moviments m
                                            join products p on (m.product_id = p.id)
                                            join stores s on (m.store_id = s.id)
                                            where m.origin <> 'api'
                                            #{@filter}
                                            group by m.product_id, m.store_id) m_entrada
                                        -- movimentacoes de saida
                                            left join (select m.store_id, m.product_id, sum(m.quantity) quantity
                                            from moviments m
                                            join stores s on (m.store_id = s.id)
                                            join products p on (m.product_id = p.id)
                                            where m.origin = 'api'
                                            #{@filter}
                                            group by m.product_id, m.store_id) m_saida on (m_entrada.store_id = m_saida.store_id and m_entrada.product_id = m_saida.product_id)
                                        join stores s on (m_entrada.store_id = s.id)
                                        join products p on (m_entrada.product_id = p.id)
                                        group by s.name, p.name")

     @chart_entrada_saida_produto = Dashboard.cubo_uma_serie(entrada_saida_produto, "Quantidade do Produto na Loja", "produto_loja", "Quantidade do Produto", "Quantidade", "quantidade_em_loja")

     valor_mensal_loja = ActiveRecord::Base.connection
                             .execute("select s.name, concat(EXTRACT(YEAR FROM m.created_at), concat('-', EXTRACT(MONTH FROM m.created_at))) mes_ano, sum(m.price_sell * m.quantity) valor_venda
                                       from moviments m
                                       join stores s on s.id = m.store_id
                                       join products p on (m.product_id = p.id)
                                       where m.origin = 'api'
                                       #{@filter}
                                       group by s.name, concat(EXTRACT(YEAR FROM m.created_at), concat('-', EXTRACT(MONTH FROM m.created_at)))
                                       order by concat(EXTRACT(YEAR FROM m.created_at), concat('-', EXTRACT(MONTH FROM m.created_at)))")




     @chart_valor_mensal_loja = Dashboard.cubo_uma_serie_concat(valor_mensal_loja, "Valor vendido por Loja", "name", "mes_ano", "Loja na Data", "Valor", "valor_venda", "#adadad")


  end

  private

  def set_filter

    @filter = ""
    @category = params[:filter][:category] rescue nil
    @unity = params[:filter][:unity] rescue nil
    @product_id = params[:filter][:product_id] rescue nil
    @store_id = params[:filter][:store_id] rescue nil

    if params[:filter]

      unless params[:filter][:data_inicio].blank? && params[:filter][:data_fim].blank?
        @data_inicio = params[:filter][:data_inicio]
        @data_fim = params[:filter][:data_fim]
        @filter += " and m.created_at between '#{params[:filter][:data_inicio]}' and '#{params[:filter][:data_fim]}'"
      end

      @filter += " and p.category = '#{params[:filter][:category]}'" unless params[:filter][:category].blank?
      @filter += " and p.unity = '#{params[:filter][:unity]}'" unless params[:filter][:unity].blank?
      @filter += " and m.product_id = #{params[:filter][:product_id]}" unless params[:filter][:product_id].blank?
      @filter += " and m.store_id = #{params[:filter][:store_id]}" unless params[:filter][:store_id].blank?

      #{"data_inicio"=>"2020-01-31", "data_fim"=>"", "category_id"=>"2", "unity_id"=>"", "product_id"=>"1", "store_id"=>""}

    end
  end

end
