class MovimentsController < ApplicationController
  before_action :set_moviment, only: [:show, :edit, :update, :destroy]

  # GET /moviments
  # GET /moviments.json
  def index

    if params[:filter].present? && params[:filter][:data_inicio].present? && params[:filter][:data_fim].present?

      @data_fim = params[:filter][:data_fim]
      @data_inicio = params[:filter][:data_inicio]

      @moviments = Moviment.where("created_at::date between '#{params[:filter][:data_inicio]}' and '#{params[:filter][:data_fim]}'").order(created_at: :desc)

    else
      @moviments = Moviment.where("created_at::date >= '#{DateTime.now.to_date}'::date - 10 ")
    end

  end

  # GET /moviments/1
  # GET /moviments/1.json
  def show
  end

  # GET /moviments/new
  def new
    @moviment = Moviment.new
  end

  # GET /moviments/1/edit
  def edit
  end

  # POST /moviments
  # POST /moviments.json
  def create
    @moviment = Moviment.new(moviment_params)
    @moviment.origin = 'created'
    respond_to do |format|
      if @moviment.save
        format.html { redirect_to @moviment, notice: I18n.t('sucesso') }
        format.json { render :show, status: :created, location: @moviment }
      else
        format.html { render :new }
        format.json { render json: @moviment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /moviments/1
  # PATCH/PUT /moviments/1.json
  def update

    @moviment.product_id = params[:product_id]
    @moviment.store_id = params[:store_id]
    @moviment.origin = 'fixed'

    respond_to do |format|
      if @moviment.update(moviment_params)
        format.html { redirect_to @moviment, notice: I18n.t('sucesso') }
        format.json { render :show, status: :ok, location: @moviment }
      else
        format.html { render :edit }
        format.json { render json: @moviment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /moviments/1
  # DELETE /moviments/1.json
  def destroy
    @moviment.destroy
    respond_to do |format|
      format.html { redirect_to moviments_url, notice: I18n.t('deleted') }
      format.json { head :no_content }
    end
  end


  def importar

    ActiveRecord::Base.transaction do

      file = open_file(params[:file][:arquivo])
      erro = {}

      (2..file.last_row).each do |i|

        if (file.row(i)[0].present?)

          nome_produto = file.row(i)[0]
          nome_loja = file.row(i)[1]
          quantidade = file.row(i)[2]
          price_sell = file.row(i)[3]
          price_cost = file.row(i)[4]
          origem = 'created'

          Moviment.create!(product_id: Product.find_by_name(nome_produto).id,
                            store_id: Store.find_by_name(nome_loja).id,
                            quantity: quantidade,
                            price_sell: price_sell,
                            price_cost: price_cost,
                            origin: origem
          )
        end
      end
    end

    flash[:success] = t("sucesso")
  rescue Exception => e
    flash[:erro] = e.message
  ensure
    redirect_to moviments_path, notice: I18n.t('importar_sucesso')
  end


  private

  # Use callbacks to share common setup or constraints between actions.
  def set_moviment
    @moviment = Moviment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def moviment_params
    params.require(:moviment).permit(:product_id, :store_id, :quantity, :price_sell, :price_cost)
  end

  def open_file(file)
    case File.extname(file.original_filename)
    when ".csv" then
      Roo::CSV.new(file.path, options = {})
    when ".xls" then
      Roo::Excel.new(file.path, options = {})
    when ".xlsx" then
      Roo::Excelx.new(file.path, options = {})
    else
      raise "Tipo de arquivo não suportado: #{file.original_filename}"
    end
  end

end
