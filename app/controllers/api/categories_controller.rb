module Api
    class CategoriesController < ApiController
        def index
            render json: Category.all
        end    
    end
end