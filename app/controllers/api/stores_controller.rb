module Api
  class StoresController < ApiController

    def index
      render json: Store.all
    end

    def show
      render json: Store.find(params[:id])
    end

  end
end