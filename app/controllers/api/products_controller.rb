module Api
  class ProductsController < ApiController
    #protect_from_forgery with: :null_session

    def index
      result = []

      produtos = Product.all
      produtos = Product.where(category: params[:category]) unless (params[:category].blank?)

      produtos.each do |product|

        avatar = Base64.encode64(File.read(product.avatar.path)) rescue ""

        result << {
            "id" => "#{product.id}",
            "name" => "#{product.name}",
            "description" => "#{product.description}",
            "price_sell" => "#{product.price_sell}",
            "unity" => "#{product.unity}",
            "category" => "#{product.category}",
            "avatar" => avatar
        }.to_h

      end

      render json: result
    end

    def show

      result = Product.find_by(id: params[:id])

      # result = {
      #     "id" => "#{result.id}",
      #     "name" => "#{result.name}",
      #     "description" => "#{result.description}",
      #     "price_sell" => "#{result.price_sell}",
      #     "unity" => "#{result.unity}",
      #     "avatar" => "#{Base64.encode64(File.read(result.avatar.path)) unless result.avatar.file.nil?}"
      # }

      unless result.nil?
        render json: result 
      else
        render json: { status: 404, messages: "Product Not Found - #{params[:id]}" }, status: 404
      end

    end

    def buy

      ids = []

      ActiveRecord::Base.transaction do
        params[:buy].each do |buy|
          product = Product.find(buy[:id])

          unless product.nil?
            moviment = Moviment.create!(
                product_id: product.id,
                store_id: buy[:store_id],
                quantity: buy[:quantity],
                price_sell: product.price_sell,
                price_cost: product.price_cost,
                origin: 'api'
            )

            ids << moviment.id

          end
        end
      end

      render json: ids

    end

  end
end