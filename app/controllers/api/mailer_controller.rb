module Api
  class MailerController < ApiController

    def send_mail
      ReceiptMailer.receipt_email(params[:email],params[:moviment_ids]).deliver_now
    end

    private
    # Never trust parameters from the scary internet, only allow the white list through.
    def mail_params
      params.require(:mail).permit(:email, :moviment_ids[])
    end

  end
end