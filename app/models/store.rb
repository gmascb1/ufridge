class Store < ApplicationRecord
  has_many :products
  # audited max_audits: 100
end
