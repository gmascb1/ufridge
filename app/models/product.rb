class Product < ApplicationRecord
  #audited max_audits: 100

  mount_uploader :avatar, AvatarUploader

  validate :validate_price, on: [:update, :save]
  validates :price_cost, presence: true
  validates :price_sell, presence: true
  validates :name, presence: true
  validates :category, presence: true


  def validate_price
    errors.add(:price_sell, I18n.t('deve ser maior que price_cost')) if self.price_cost >= self.price_sell
  end

end
