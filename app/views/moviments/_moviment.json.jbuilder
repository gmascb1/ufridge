json.extract! moviment, :id, :product_id, :store_id, :quantity, :price_sell, :price_cost, :created_at, :updated_at
json.url moviment_url(moviment, format: :json)
