class Dashboard

  def self.cubo_uma_serie_concat(data, titulo, nome_x, nome_x2, nome_serie, nomeY, nome_map, cor = "#1c84c6")

    cubo = LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: titulo)
      f.xAxis(categories: data.map { |i| "#{i[nome_x].to_s} - #{i[nome_x2].to_s}" })
      f.series(name: nome_serie, yAxis: 0, data: data.map { |i| i[nome_map].to_i }, color: cor)
      f.yAxis [
                  {title: {text: nomeY, margin: 70}},
              ]
      f.legend(reversed: true, borderWidth: 0)
      f.chart({defaultSeriesType: "column"})
    end

    cubo

  end

  def self.cubo_uma_serie(data, titulo, nome_x, nome_serie, nomeY, nome_map, cor = "#1c84c6")

    cubo = LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: titulo)
      f.xAxis(categories: data.map { |i| i[nome_x].to_s })
      f.series(name: nome_serie, yAxis: 0, data: data.map { |i| i[nome_map].to_i }, color: cor)
      f.yAxis [
                  {title: {text: nomeY, margin: 70}},
              ]
      f.legend(reversed: true, borderWidth: 0)
      f.chart({defaultSeriesType: "column"})
    end

    cubo

  end


  def cubo_duas_series(data, titulo, nome_x, nome_serie1, nome_serie2, nomeY)

    cubo = LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: titulo)
      f.xAxis(categories: data.map { |i| i[nome_x] })
      f.series(name: nome_serie1, yAxis: 0, data: data.map { |i| i['entrada'].to_i }, color: "#1c84c6")
      f.series(name: nome_serie2, yAxis: 0, data: data.map { |i| i['saida'].to_i }, color: '#24a90e')
      f.yAxis [
                  {title: {text: nomeY, margin: 70}},
              ]
      f.legend(reversed: true, borderWidth: 0,)
      f.chart({defaultSeriesType: "column"})
    end

    cubo

  end



  private

  def concat(string1, string2)
    return string1 + " - " + string2
  end

end