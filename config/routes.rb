Rails.application.routes.draw do
  resources :moviments
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  resources :categories
  resources :unities
  resources :products
  resources :stores
  get 'home/index'
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'home#index'

  post 'importar' => 'moviments#importar', as: :importar

  namespace :api do

    get 'products' => 'products#index', as: :products_index
    get 'products/:id' => 'products#show', as: :products_show

    get 'stores' => 'stores#index', as: :stores_index
    get 'stores/:id' => 'stores#show', as: :stores_show

    get 'categories' => 'categories#index', as: :categories_index

    post 'products/buy' => 'products#buy', as: :buy

    post 'mailer/send_mail' => 'mailer#send_mail', as: :send_mail

  end


end
